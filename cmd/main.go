package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"

	"../pkg/controller"
	"../pkg/service"

	"github.com/gorilla/mux"
)

func main() {

	port := flag.String("port", "", "server port")
	flag.Parse()

	portNum, err := strconv.Atoi(*port)
	if err != nil || *port == "" || portNum < 1 || portNum > 65535 {
		log.Printf("Missing or invalid port argument passed [%s]. Default port 8080 will be used", *port)
		*port = "8080"
	}

	translationService := service.NewTranslationService()
	wordHandler := &controller.WordRequestHandler{WordTranslationService: translationService}
	sentenceHandler := &controller.SentenceRequestHandler{SentenceTranslationService: translationService}
	historyHandler := &controller.TranslationHistoryHandler{HistoryManager: translationService}

	router := mux.NewRouter()
	router.Handle("/word", wordHandler).Methods(http.MethodPost)
	router.Handle("/sentence", sentenceHandler).Methods(http.MethodPost)
	router.Handle("/history", historyHandler).Methods(http.MethodGet)

	server := &http.Server{
		Addr:         fmt.Sprintf(":%s", *port),
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 60 * time.Second,
		IdleTimeout:  5 * time.Second,
		Handler:      router,
	}

	go func() {
		log.Printf("Starting HTTP server. Listening on port %s", *port)
		err := server.ListenAndServe()
		if err != nil {
			log.Println("Error setting up HTTP server: ", err)
			return
		}
	}()

	shutdown := make(chan os.Signal, 1)
	defer close(shutdown)
	signal.Notify(shutdown, os.Interrupt)
	<-shutdown
	log.Println("Stopping server")

}
