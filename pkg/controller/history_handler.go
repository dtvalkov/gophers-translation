package controller

import (
	"log"
	"net/http"

	"../model"
)

type TranslationHistoryHandler struct {
	HistoryManager HistoryManager
}

type HistoryManager interface {
	GetHistory() (model.TranslationHistoryResponse, error)
}

func (t *TranslationHistoryHandler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {

	history, err := t.HistoryManager.GetHistory()
	if err != nil {
		log.Printf("error translating word: %v", err)
		handleErrorHTTPResponse(writer, http.StatusInternalServerError, "error processing request")
		return
	}
	handleHTTPResponse(writer, history)
}
