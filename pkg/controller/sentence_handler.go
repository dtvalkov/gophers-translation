package controller

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"../model"
)

type SentenceRequestHandler struct {
	SentenceTranslationService SentenceTranslationService
}

type SentenceTranslationService interface {
	TranslateSentence(sentence model.Sentence) (model.SentenceTranslationResponse, error)
}

func (t *SentenceRequestHandler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {

	var requestBody model.SentenceTranslationRequest
	decoder := json.NewDecoder(request.Body)
	err := decoder.Decode(&requestBody)
	if err != nil {
		handleErrorHTTPResponse(writer, http.StatusBadRequest, "Invalid request body")
		return
	}
	sentence, err := validateSentence(requestBody.Sentence)
	if err != nil {
		handleErrorHTTPResponse(writer, http.StatusBadRequest, err.Error())
		return
	}

	response, err := t.SentenceTranslationService.TranslateSentence(sentence)
	if err != nil {
		log.Printf("error translating sentence: %v", err)
		handleErrorHTTPResponse(writer, http.StatusInternalServerError, "error processing request")
		return
	}
	handleHTTPResponse(writer, response)
}

func validateSentence(sentence string) (model.Sentence, error) {
	result := model.Sentence{}
	if sentence == "" {
		return result, fmt.Errorf("A non-empty sentence is required")
	}
	lastRune := sentence[len(sentence)-1:]
	if lastRune != "!" && lastRune != "?" && lastRune != "." {
		return result, fmt.Errorf("A valid sentence ending with a dot, exclamation or question mark is required")
	}
	sentence = strings.ToLower(sentence)
	sentence = strings.TrimRight(sentence, lastRune)
	sentence = strings.TrimSpace(sentence)

	result.Words = make([]string, 0)
	result.PunctuationMark = lastRune
	result.StrippedSentence = sentence

	words := strings.Split(sentence, " ")
	for _, word := range words {
		if !WordValidationPattern.MatchString(word) {
			return result, fmt.Errorf("Invalid word [%s]. Only letters and hyphens are allowed", word)
		}
		result.Words = append(result.Words, word)
	}

	return result, nil
}
