package controller

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"../model"
)

type WordRequestHandler struct {
	WordTranslationService WordTranslationService
}

type WordTranslationService interface {
	TranslateWord(word string) (model.WordTranslationResponse, error)
}

func (t *WordRequestHandler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {

	var requestBody model.WordTranslationRequest
	decoder := json.NewDecoder(request.Body)
	err := decoder.Decode(&requestBody)
	if err != nil {
		handleErrorHTTPResponse(writer, http.StatusBadRequest, "Invalid request body")
		return
	}
	word, err := validateWord(requestBody.Word)
	if err != nil {
		handleErrorHTTPResponse(writer, http.StatusBadRequest, err.Error())
		return
	}
	response, err := t.WordTranslationService.TranslateWord(word)
	if err != nil {
		log.Printf("error translating word: %v", err)
		handleErrorHTTPResponse(writer, http.StatusInternalServerError, "error processing request")
		return
	}
	handleHTTPResponse(writer, response)
}

func validateWord(word string) (string, error) {
	word = strings.TrimSpace(word)
	if len(strings.Split(word, " ")) > 1 {
		return "", fmt.Errorf("A single word is required.")
	}
	if !WordValidationPattern.MatchString(word) {
		return "", fmt.Errorf("A valid word is required. Only letters and hyphens are allowed.")
	}
	return word, nil
}
