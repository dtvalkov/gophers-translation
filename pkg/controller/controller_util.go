package controller

import (
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"time"
)

var WordValidationPattern *regexp.Regexp = regexp.MustCompile("^[a-zA-Z]+(-[a-zA-Z]+)*$") //only letters and hyphens

type httpError struct {
	RespMessage string    `json:"message"`
	RespCode    int       `json:"code"`
	RespError   string    `json:"error"`
	Timestamp   time.Time `json:"timestamp"`
}

func handleErrorHTTPResponse(writer http.ResponseWriter, code int, message string) {
	jsonResp, _ := json.Marshal(httpError{
		RespMessage: message,
		RespCode:    code,
		RespError:   http.StatusText(code),
		Timestamp:   time.Now(),
	})
	writer.Header().Add("Content-Type", "application/json")
	writer.WriteHeader(code)
	writer.Write(jsonResp)
	return
}

func handleHTTPResponse(writer http.ResponseWriter, dataOut interface{}) error {
	if dataOut == nil {
		return fmt.Errorf("empty output")
	}
	resp, err := json.Marshal(dataOut)
	if err != nil {
		handleErrorHTTPResponse(writer, http.StatusInternalServerError, err.Error())
		return err
	}
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(http.StatusOK)
	_, err = writer.Write(resp)
	return err
}
