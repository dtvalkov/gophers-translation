package controller

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"../model"
	"github.com/stretchr/testify/assert"
)

func TestWordHandler_ServeHTTP(t *testing.T) {
	type fields struct {
		Service WordTranslationService
	}
	type args struct {
		request string
	}
	tests := []struct {
		name           string
		wantStatusCode int
		fields         fields
		args           args
	}{
		{
			name:           "translate-word-no-request-body",
			wantStatusCode: 400,
			fields: fields{
				Service: &MockWordTranslationService{
					Response: model.WordTranslationResponse{},
					Error:    nil,
				},
			},
			args: args{
				request: ``,
			},
		},
		{
			name:           "translate-word-empty-word",
			wantStatusCode: 400,
			fields: fields{
				Service: &MockWordTranslationService{
					Response: model.WordTranslationResponse{},
					Error:    nil,
				},
			},
			args: args{
				request: `{"english-word" : ""}`,
			},
		},
		{
			name:           "translate-word-multiple-words",
			wantStatusCode: 400,
			fields: fields{
				Service: &MockWordTranslationService{
					Response: model.WordTranslationResponse{},
					Error:    nil,
				},
			},
			args: args{
				request: `{"english-word" : "multiple words"}`,
			},
		},
		{
			name:           "translate-word-processing-error",
			wantStatusCode: 500,
			fields: fields{
				Service: &MockWordTranslationService{
					Response: model.WordTranslationResponse{},
					Error:    fmt.Errorf("some service error"),
				},
			},
			args: args{
				request: `{"english-word" : "word"}`,
			},
		},
		{
			name:           "translate-word-valid",
			wantStatusCode: 200,
			fields: fields{
				Service: &MockWordTranslationService{
					Response: model.WordTranslationResponse{},
					Error:    nil,
				},
			},
			args: args{
				request: `{"english-word" : "chair"}`,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &WordRequestHandler{WordTranslationService: tt.fields.Service}
			req, err := http.NewRequest("POST", "/word", strings.NewReader(tt.args.request))
			if err != nil {
				t.Fatal(err)
			}
			rr := httptest.NewRecorder()
			handlerFunc := func(w http.ResponseWriter, r *http.Request) {
				h.ServeHTTP(w, r)
			}
			http.HandlerFunc(handlerFunc).ServeHTTP(rr, req)
			assert.Equal(t, tt.wantStatusCode, rr.Code, string(rr.Body.Bytes()))
		})
	}

}

type MockWordTranslationService struct {
	Response model.WordTranslationResponse
	Error    error
}

func (m *MockWordTranslationService) TranslateWord(word string) (model.WordTranslationResponse, error) {
	return m.Response, m.Error
}
