package service

import (
	"fmt"
	"strings"

	"../model"
)

type TranslationService struct {
	History map[string]string
}

func NewTranslationService() *TranslationService {
	return &TranslationService{
		History: make(map[string]string),
	}
}

func (s *TranslationService) TranslateWord(word string) (model.WordTranslationResponse, error) {
	var translated string
	var err error
	result := model.WordTranslationResponse{}
	word = strings.ToLower(word)

	if val, ok := s.History[word]; ok {
		translated = val
	} else {
		translated, err = translateWord(word)
		if err != nil {
			return result, err
		}
		s.History[word] = translated
	}
	result.Word = translated
	return result, nil
}

func (s *TranslationService) TranslateSentence(sentence model.Sentence) (model.SentenceTranslationResponse, error) {
	var sentenceBuilder strings.Builder
	totalWords := len(sentence.Words)
	result := model.SentenceTranslationResponse{}

	if val, ok := s.History[sentence.StrippedSentence]; ok {
		result.Sentence = fmt.Sprintf("%s%s", val, sentence.PunctuationMark)
		return result, nil
	}

	for index, word := range sentence.Words {
		var toAdd string
		if val, ok := s.History[word]; ok {
			toAdd = val
		} else {
			toAdd, _ = translateWord(word)
		}
		sentenceBuilder.WriteString(toAdd)
		if index < totalWords-1 {
			sentenceBuilder.WriteString(" ")
		}
	}
	s.History[sentence.StrippedSentence] = sentenceBuilder.String()
	result.Sentence = fmt.Sprintf("%s%s", sentenceBuilder.String(), sentence.PunctuationMark)

	return result, nil
}

func (s *TranslationService) GetHistory() (model.TranslationHistoryResponse, error) {
	response := model.TranslationHistoryResponse{}
	response.History = make([]map[string]string, 1)
	response.History[0] = s.History
	return response, nil
}

func translateWord(word string) (string, error) {
	var first rune
	for _, c := range word {
		first = c
		break
	}
	if IsVowel(first) {
		return fmt.Sprintf("g%s", word), nil
	} else {
		if strings.HasPrefix(word, "xr") {
			return fmt.Sprintf("ge%s", word), nil
		} else {
			return translateWordWithConsonantPrefix(word)
		}
	}
}

func translateWordWithConsonantPrefix(word string) (string, error) {
	var prefixBuilder strings.Builder
	qDetected := false

	for _, c := range word {
		if !IsVowel(c) {
			prefixBuilder.WriteString(string(c))
			if c == 'q' {
				qDetected = true
			}
		} else {
			if qDetected {
				if c == 'u' {
					prefixBuilder.WriteString("u")
				}
			}
			break
		}
	}
	prefix := prefixBuilder.String()
	return fmt.Sprintf("%s%sogo", strings.TrimPrefix(word, prefix), prefix), nil
}
