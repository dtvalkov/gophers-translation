package service

import "unicode"

var vowels = map[rune]bool{
	'a': true,
	'e': true,
	'i': true,
	'o': true,
	'u': true,
	'y': true,
}

func IsVowel(r rune) bool {
	r = unicode.ToLower(r)
	return vowels[r]
}
