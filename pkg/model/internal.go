package model

type Sentence struct {
	Words            []string
	PunctuationMark  string
	StrippedSentence string
}
