package model

type WordTranslationResponse struct {
	Word string `json:"gopher-word"`
}

type SentenceTranslationResponse struct {
	Sentence string `json:"gopher-sentence"`
}

type TranslationHistoryResponse struct {
	History []map[string]string `json:"history"`
}
