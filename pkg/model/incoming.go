package model

type WordTranslationRequest struct {
	Word string `json:"english-word"`
}

type SentenceTranslationRequest struct {
	Sentence string `json:"english-sentence"`
}
